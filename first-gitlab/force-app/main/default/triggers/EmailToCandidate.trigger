trigger EmailToCandidate on Candidate_Sanam__c (before insert,after update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            Set<Id> CandidateIds = new Set<Id>();
            for(Candidate_Sanam__c CandiT : Trigger.new){
                if(CandiT.Status__c == 'Hired' ){
                    CandidateIds.add(CandiT.Id);
                }
            }
            SendEmail.SendToCandidate(CandidateIds);
        }
    }
}