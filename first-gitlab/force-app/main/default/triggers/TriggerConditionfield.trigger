trigger TriggerConditionfield on Account (before insert,after insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            TriggerAutoClass.AutoMethod(Trigger.new);
        }
        else if(Trigger.isAfter){
            TriggerAutoClass.createRelatedOpp(Trigger.new);
        }  
    }
}