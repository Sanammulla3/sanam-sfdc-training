trigger JobTrigger on Job_Sanam__c (before delete,after update,before update) {
    //TriggerSetting__c Tobj = TriggerSetting__c.getInstance('JobTrigger'); //custom setting
    //if(cObj.IsActive__c==true){       
    //Task3
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            JobTriggerHandler.jobStatusActiveNotDeletedError(Trigger.old);  //Task3
        }
    }
    
    //Task4
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            JobTriggerHandler.numberCandidatesHiredEqualsNumberPositionsDeactivatedCandidatesApplyJob(Trigger.new); //Task4
            JobTriggerHandler.autoActivatedNoPositionsGreaterThanHire(Trigger.new); //Task6
        }
    }
    
    //Task5
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            JobTriggerHandler.positionsJobEqualNumberHiredCandidatesEmail(Trigger.new);
        }
    }
    //}      
}