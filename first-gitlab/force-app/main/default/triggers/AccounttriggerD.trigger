trigger AccounttriggerD on Account (before insert,after insert,before update) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
             AccountTriggerHandler.MethodName(Trigger.new);
        }
        else if(Trigger.isAfter){
            //code
        }
    }
    else if(Trigger.isUpdate){
        //code
    }
    
}