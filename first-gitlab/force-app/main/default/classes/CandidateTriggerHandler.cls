public class CandidateTriggerHandler {
    //--------------//Task1--------------
    public static void candidateMissedEnterExpectedSalaryError (List<Candidate_Sanam__c> lstCandidate){          
        for(Candidate_Sanam__c objCadidate : lstCandidate){ 
            if(objCadidate.Expected_Salary__c == null){
                objCadidate.addError('Expected Salary field is missing');
            }                                      	
        }
    }  
    
    public static void candidateApplyingJobStatusNotActiveError (List<Candidate_Sanam__c> lstCandidate){ 
        List<Id> candidateIds = new List<Id>();
        for(Candidate_Sanam__c objCandidate : lstCandidate){
            candidateIds.add(objCandidate.Job_Sanam__c);           
        }
         
        Map<ID, Job_Sanam__c> objMap = new Map<ID, Job_Sanam__c>([SELECT Id,Active__c FROM Job_Sanam__c WHERE id IN : candidateIds AND Active__c = false]);

        for(Candidate_Sanam__c objCan : lstCandidate){
            if(objMap.containsKey(objCan.Job_Sanam__c)){
                objCan.addError('Job Is InActive Cannot Apply');
            }
        }
    } 
    
    //---------------------//Task2---------------------------    
    public static void createdApplicationDateAdd(List<Candidate_Sanam__c> litCandidate){
        /*for(Candidate_Sanam__c objCandidate : litCandidate){
            if(objCandidate.Application_Date__c != System.today() || objCandidate.Application_Date__c == null){
                objCandidate.Application_Date__c = objCandidate.CreatedDate.date();     
            }
            insert objCandidate;
        }*/
        
        List<Candidate_Sanam__c > lstCandidate1 = new List<Candidate_Sanam__c >();
        for(Candidate_Sanam__c  objCandidate : [SELECT id,name,Application_Date__c, CreatedDate FROM Candidate_Sanam__c  WHERE ID IN : litCandidate ]){           
            if(objCandidate.Application_Date__c == null || objCandidate.Application_Date__c > objCandidate.CreatedDate.date() || objCandidate.Application_Date__c <objCandidate.CreatedDate.date()){              
                objCandidate.Application_Date__c = objCandidate.CreatedDate.date();                
                lstCandidate1.add(objCandidate);                
            }            
        }
        update lstCandidate1;
        system.debug('lstCandidate1'+lstCandidate1);
    }
}