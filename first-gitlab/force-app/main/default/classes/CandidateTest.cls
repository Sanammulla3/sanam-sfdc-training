@isTest
public class CandidateTest {
    static TriggerSetting__c setting = new TriggerSetting__c();
    
    @isTest(SeeAllData=true)
    static void setup(){
        setting.Name = 'TriggerSetting';
        setting.IsActive__c = true;
        insert setting;	
        system.debug('setting' +setting);
        
        List<Job_Sanam__c> jobList = new  List<Job_Sanam__c>();
        List<Job_Sanam__c> jobListWithPositionZero = new  List<Job_Sanam__c>();
        	
        Contact contactObj = new Contact();
        contactObj.LastName = 'Manager2';
        insert contactObj;
        System.debug('contactObj' + contactObj.Id);
        
        jobList.add(new Job_Sanam__c(Name = 'testJob',
                                     Number_of_Positions__c = 5,
                                     Active__c = true,
                                     Salary_Offered__c = 10000,
                                     Manager__c = contactObj.id,
                                     Job_Type__c = 'Manager',
                                     Required_Skills__c = 'People Skills',
                                     Qualification_Required__c = 'Bcom',
                                     Certification_Required__c = 'CMP (Certified Project Manager)'
                                    ));            
        
        insert jobList;
        System.debug('jobList' + jobList); 
        
        jobListWithPositionZero.add(new Job_Sanam__c(Name = 'NPZero',
                                                     Number_of_Positions__c = 1,
                                                     Active__c = true,
                                                     Salary_Offered__c = 10000,
                                                     Manager__c = contactObj.id,
                                                     Job_Type__c = 'Manager',
                                                     Required_Skills__c = 'People Skills',
                                                     Qualification_Required__c = 'Bcom',
                                                     Certification_Required__c = 'CMP (Certified Project Manager)'
                                                    ));
        insert jobListWithPositionZero;
        System.debug('jobListWithPositionZero' + jobListWithPositionZero); 
    }
    
    
    public static testMethod void checkSalary(){
        List<Candidate_Sanam__c> lstCandidate = new List<Candidate_Sanam__c>();
        List<Job_Sanam__c> litJob  = [SELECT id FROM Job_Sanam__c WHERE Name = 'testJob' Limit 1]; 
        system.debug('litJob' +litJob); 
        
        Id jobID = litJob[0].id;
        system.debug('jobID' +jobID);
        
        for(Integer i = 0 ; i<5 ; i++) {
            lstCandidate.add(new Candidate_Sanam__c(First_Name__c='test'+i,
                                                    Last_Name__c='test1'+i,
                                                    Email__c='test'+i+'@gmail.com',
                                                    Status__c='applied',
                                                    Job_Sanam__c = jobID,
                                                    Expected_Salary__c = NULL,
                                                    Country__c = 'india',
                                                    State__c = 'Gujrat'                                                 
                                                   ));
            
        }       
        insert lstCandidate; 
        Test.startTest();
        Database.SaveResult[] result = Database.insert(lstCandidate, false);
        System.debug(result);
        Test.stopTest();
        for (Database.SaveResult sr : result) {
            System.assertEquals(false, sr.isSuccess());
            //assertEquals(expected[i],actual[i])
            System.assertEquals('Expected Salary field is missing',sr.getErrors()[0].getMessage());
        }  
        
        System.debug('lstCandidate' + lstCandidate);     
    }
    
    
    public static testMethod void appDateUnitTest(){
        List<Candidate_Sanam__c> cnList=new List<Candidate_Sanam__c>();
        Candidate_Sanam__c cnObj=new Candidate_Sanam__c();
        cnObj.Application_Date__c = System.today();
        
        cnList.add(cnObj);
        Test.startTest();    
        Database.SaveResult[] sr = Database.insert(cnList,false);
        Test.stopTest();
    }

}