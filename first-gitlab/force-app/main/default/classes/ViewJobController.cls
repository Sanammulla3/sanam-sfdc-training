public class ViewJobController {
    public String getDynamiclist() {
        return null;
    }   
    public Candidate_Sanam__c newCandidate{
        get;
        private set;
    }    
    public List<sObject> records{get; set;}
    public List<String> fields{get; set;}   
    public List<Candidate_Sanam__c> candidateRecords{get; set;}   
    public List<String> candidateFields{get; set;}
    public List<String> filterid{get; set;}
    public List<Job_Sanam__c> job{get; set;}    
    public ViewJobController(){
        Id id= ApexPages.currentPage().getParameters().get('id');       
        records=[select Name, Job_Type__c,  Number_of_Positions__c, Qualification_Required__c, Required_Skills__c,
                 Salary_Offered__c, Certification_Required__c from Job_Sanam__c where Id=:id];    
        
        fields= new List<String>{'Name', 'Job_Type__c',  'Number_of_Positions__c', 'Qualification_Required__c', 'Required_Skills__c',
            'Salary_Offered__c', 'Certification_Required__c'};               
                job= [select Job_Type__c from Job_Sanam__c where Id=:id ];
        
        candidateRecords=[SELECT First_Name__c, Last_Name__c, Email__c, Status__c, Job_Sanam__c FROM Candidate_Sanam__c WHERE Job_Sanam__c=:job];  
    }   
    
    public PageReference save() {
        try{
            upsert(newCandidate);
        }
        catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        PageReference redirectSuccess = new ApexPages.StandardController(newCandidate).View();
        return redirectSuccess;
    }
}