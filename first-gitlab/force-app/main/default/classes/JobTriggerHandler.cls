public class JobTriggerHandler {
    //---------------------//Task3---------------------------      
    public static void jobStatusActiveNotDeletedError(List<Job_Sanam__c> lstJobSanam){ 
        for(Job_Sanam__c objJob : lstJobSanam){          
            if(objJob.Active__c){
                objJob.addError('This Job is active and can not be deleted...!');                
            }
        }        
    }
    
    //---------------------//Task4---------------------------      
    public static void numberCandidatesHiredEqualsNumberPositionsDeactivatedCandidatesApplyJob(List<Job_Sanam__c> lstJob){
        for(Job_Sanam__c objJob : lstJob){   
            if(objJob.Number_of_Positions__c <= objJob.Hired_Applicants__c){
                objJob.Active__c = false;
            }
        }
    }
    
    //---------------------//Task5-------------------------- 
    public static void positionsJobEqualNumberHiredCandidatesEmail(List<Job_Sanam__c> lstJob){
        List<Id> managerIds = new List<Id>();
        
        for(Job_Sanam__c objJob : lstJob){
            if(objJob.Number_of_Positions__c <= objJob.Hired_Applicants__c){
                managerIds.add(objJob.Manager__c);
            }
        }
        system.debug('managerIds' +managerIds);
        List<Contact> objContact  = ([SELECT Email FROM Contact WHERE id IN : managerIds]);
        system.debug('objContact' +objContact);
        
        for(Contact objJobEmailSend : objContact){   
            system.debug('objJobEmailSend' +objJobEmailSend);
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[]{objJobEmailSend.Email};
            message.optOutPolicy = 'FILTER';
            message.subject = 'Reached Vaccancy Limit!..';
            message.plainTextBody = 'All required candidate has been hired for this job. ';
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }        
        }
    }
    
    
    //---------------------//Task6--------------------------      
    public static void autoActivatedNoPositionsGreaterThanHire(List<Job_Sanam__c> listJob){
        for(Job_Sanam__c objJob:listJob){
            if(objJob.Number_of_Positions__c > objJob.Hired_Applicants__c ){
                objJob.Active__c=true;
            }
        }       
    }
    
}