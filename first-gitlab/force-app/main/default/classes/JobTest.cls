@isTest
private class JobTest{ 
	static TriggerSetting__c setting = new TriggerSetting__c();    
    
    @testSetup
    static void setup(){
        setting.Name = 'TriggerSetting';
        setting.IsActive__c = true;
        insert setting;	
        
        List<Job_Sanam__c> jobList = new  List<Job_Sanam__c>();
        List<Job_Sanam__c> jobListWithPositionZero = new  List<Job_Sanam__c>();
        
        Contact contactObj = new Contact();
        contactObj.LastName = 'Manager2';
        insert contactObj;
        System.debug('contactObj' + contactObj.Id);
        
        jobList.add(new Job_Sanam__c(Name = 'testJob',
                                     Number_of_Positions__c = 5,
                                     Active__c = true,
                                     Salary_Offered__c = 10000,
                                     Manager__c = contactObj.id,
                                     Job_Type__c = 'Manager',
                                     Required_Skills__c = 'People Skills',
                                     Qualification_Required__c = 'Bcom',
                                     Certification_Required__c = 'CMP (Certified Project Manager)'
                                    ));            
        
        insert jobList;
        System.debug('jobList' + jobList); 
        
        jobListWithPositionZero.add(new Job_Sanam__c(Name = 'NPZero',
                                                     Number_of_Positions__c = 1,
                                                     Active__c = true,
                                                     Salary_Offered__c = 10000,
                                                     Manager__c = contactObj.id,
                                                     Job_Type__c = 'Manager',
                                                     Required_Skills__c = 'People Skills',
                                                     Qualification_Required__c = 'Bcom',
                                                     Certification_Required__c = 'CMP (Certified Project Manager)'
                                                    ));
        insert jobListWithPositionZero;
        System.debug('jobListWithPositionZero' + jobListWithPositionZero); 
    }
    
    
    
    
    public static testMethod void numberPositionZero(){
        List<Job_Sanam__c> litJob  = new List<Job_Sanam__c>();
        litJob  = [SELECT id,Number_of_Positions__c FROM Job_Sanam__c WHERE Name = 'NPZero' Limit 1]; 
        system.debug('litJob' +litJob); 
        litJob[0].Number_of_Positions__c = 0;
        update litJob;
        Test.startTest();
        litJob  = [SELECT id,Number_of_Positions__c,Active__c FROM Job_Sanam__c WHERE Name = 'NPZero' Limit 1]; 
        System.debug('litJob' + litJob);
        System.assertEquals(false, litJob[0].Active__c);
        Test.stopTest();
    }
    
    public static testMethod void appDateUnitTest(){
        List<Candidate_Sanam__c> cnList=new List<Candidate_Sanam__c>();
        Candidate_Sanam__c cnObj=new Candidate_Sanam__c();
        cnObj.Application_Date__c = System.today();
        
        cnList.add(cnObj);
        Test.startTest();    
        Database.SaveResult[] sr = Database.insert(cnList,false);
        Test.stopTest();
    }
    
    //for job
    public static testMethod void preventDeleteUnitTest(){        
        Test.startTest();
        try{
            List<Job_Sanam__c> jobList=[select Active__c from Job_Sanam__c where Active__c = true];
            Delete jobList;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Delete failed.'));
            system.assert(message.contains('This Job is active and can not be deleted'));
        }
        Test.stopTest();
    }
    
    
    //for job
    public static testMethod void deleteUnitTest(){
        Job_Sanam__c jobObj = new Job_Sanam__c();
        jobObj.Name='Test_Job';
        jobObj.Active__c = false;
        
        Candidate_Sanam__c cnObj=new Candidate_Sanam__c();
        List<Candidate_Sanam__c> cnList =new List<Candidate_Sanam__c>();
        cnObj.name = 'ajay';
        cnObj.Job_Sanam__c = jobObj.id;
        cnList.add(cnObj);
        
        Test.startTest();
        Database.SaveResult[] sr = Database.insert(cnList,false);
        Test.stopTest();
        try{
            System.assertEquals(1,sr.size());
        }
        catch(DmlException e){
            String message = e.getMessage();          
            system.assert(message.contains('This Job is not active. You can not apply for this job'));
        }
    }
    
}