global class BatchToInActiveExpireJobs implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        return Database.getQueryLocator('SELECT id,Active__c,Expires_On__c FROM Job_Sanam__c');        
    }
    global void execute(Database.BatchableContext batchContext, List<Job_Sanam__c> lstJob){
        datetime TodayDate = datetime.now();
        for(Job_Sanam__c job1 : lstJob){            
            System.debug('TodayDate' +TodayDate);
            System.debug('job1.Expires_On__c' +job1.Expires_On__c);
            if(job1.Expires_On__c == NULL || job1.Expires_On__c < TodayDate){
                job1.Active__c = false;
            }            
        }
        Database.SaveResult[] result = Database.update(lstJob, false);
    } 
    
    global void finish(Database.BatchableContext batchContext){
        
    }
    
}







//globle and public