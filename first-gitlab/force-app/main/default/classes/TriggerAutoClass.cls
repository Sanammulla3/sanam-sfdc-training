public class TriggerAutoClass {
    public static void AutoMethod(List<Account> newList){
        for(Account acc : newList){
            if(acc.Industry!=null && acc.SLA__c=='Gold'){
                acc.Rating='Hot';
            }
        }
    }
    
    public static void createRelatedOpp(List<Account> newList){
		List<Opportunity> oppToBeInserted= new List<Opportunity>();  
        for(Account acc:newList){
            Opportunity opp=new Opportunity();
            opp.Name=acc.Name;
            opp.StageName='Prospecting';
            opp.CloseDate=System.today();
            oppToBeInserted.add(opp);
        }
        if(!oppToBeInserted.isEmpty()){
            insert oppToBeInserted;
        }
    }
}