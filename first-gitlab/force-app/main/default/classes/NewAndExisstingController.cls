public class NewAndExisstingController {

    public Account account {get;public set;}
    public NewAndExisstingController(){
        Id id=ApexPages.currentPage().getPArameters().get('id');
        account=(id==null)?new Account():[select name,Phone,industry from Account where Id=:id];
        
    }
    public PageReference save(){
        try{
            upsert(account);
        }
        catch(System.DMLException e){
            ApexPAges.addMessages(e);
            return null;
        }
        PageReference redirectSuccess=new ApexPages.StandardController(Account).view();
        return redirectSuccess;
        
    }
}