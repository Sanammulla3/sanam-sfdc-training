public without sharing class AccountProcessor {
	@future
    public static void countcontacts(List<Id> accountIds){
        List<Account> accounts = [SELECT Id,(SELECT Id FROM contacts) FROM Account WHERE id IN :accountIds];
        for(Account acc: accounts){
            acc.Number_of_Contacts__c = acc.Contacts.size();
        }
        update accounts;
    }
}