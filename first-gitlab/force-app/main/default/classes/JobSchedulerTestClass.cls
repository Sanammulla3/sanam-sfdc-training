@isTest
public class JobSchedulerTestClass {
    @testSetup
    private static void setup() {
        Contact contactObj = new Contact (LastName='TestContact');
        insert contactObj;
        Date closeDate = Date.today().addDays(-7);
        System.debug('closeDate' +closeDate);
        
        List<Contact> contactlist = [SELECT id FROM Contact WHERE LastName='TestContact' LIMIT 1];
        
        System.debug('contactlist' +contactlist);
        List<Job_Sanam__c> lstJob1 = new List<Job_Sanam__c>();
        for(Integer i=0;i<10;i++){
            Job_Sanam__c jobObj = new Job_Sanam__c(Name='testJob'+i,
                                                   Number_of_Positions__c = 3,
                                                   Required_Skills__c='Team Player',
                                                   Job_Type__c='Developer' ,
                                                   Expires_On__c = System.today().addDays(-7),
                                                   Active__c=true,
                                                   Manager__c = contactlist[0].Id
                                                  );
            lstJob1.add(jobObj);           
        }
        insert lstJob1;
    }
    
    @isTest static void testScheduledJob() {
        date today = System.today();
        String cronExp = '0 44 11 ? * * *';
        List<Job_Sanam__c> jobListData = new List<Job_Sanam__c>([SELECT Id, Active__c, Expires_On__c
                                                                       FROM Job_Sanam__c WHERE Expires_On__c<today]);
        
        String jobId = System.schedule('ScheduledApexTest', cronExp, new JobSanam());
        
        // Verify the scheduled job has not run yet.
        List<Job_Sanam__c> lstJob = [SELECT Id FROM Job_Sanam__c WHERE Id IN: jobListData];
        
        System.assertEquals(10, lstJob.size(), 'Tasks exist before job has run');   
    }
}